defmodule Theopse.Mininal.O do
  @spec macro_concat(any, any) :: {:__block__, [], maybe_improper_list}
  def macro_concat(macro1, macro2)

  def macro_concat({:__block__, [], list1}, {:__block__, [], list2}) do
    {:__block__, [], Enum.concat(list1, list2)}
  end

  def macro_concat({:__block__, [], list1}, macro2) do
    {:__block__, [], Enum.concat(list1, [macro2])}
  end

  def macro_concat(macro1, {:__block__, [], list2}) do
    {:__block__, [], Enum.concat([macro1], list2)}
  end

  def macro_concat(macro1, macro2) when is_tuple(macro1) and is_tuple(macro2) do
    {:__block__, [], [macro1, macro2]}
  end
end

defmodule Theopse.Mininal do
  defmacro __using__(_) do
    quote do
      import Theopse.Mininal
    end
  end
end
