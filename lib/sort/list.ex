# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File is From Theopse Organization(mail@theopse.org)
# Licensed under BSD-2-Clause
# File: list.ex (theopse/existence/algorithms/sort.ex/lib/sort/list.ex)
# Content:
# Copyright (c) 2023 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defimpl Theopse.Algorithms.Sort, for: List do
  @moduledoc """
  Sorting algorithms by Theopse Organization
  """

  use Theopse.Mininal
  import Theopse.Algorithms.Sort.Macros

  @doc """
  Sort a given-method enumerable, using BubbleSort.

  Ascending unless giving the second argument true.

  ## Examples

      iex> Theopse.Algorithms.Sort.bubble([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0])
      [0, 0, 2, 2, 4, 4, 4, 4, 5, 7, 7]

      iex> Theopse.Algorithms.Sort.bubble([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0], true)
      [7, 7, 5, 4, 4, 4, 4, 2, 2, 0, 0]

  """
  @spec bubble(list(term)) :: list(term)
  @spec bubble(list(term), boolean) :: list(term)
  def bubble(enum, desc? \\ false)
  def bubble(enum, false), do: bubble_p(enum)
  def bubble(enum, true), do: bubble(enum, false) |> Enum.reverse()

  dpws(bubble_p(origin = [pivot | rest]), do: bubble_p(rest, pivot, [], [], origin))

  defp bubble_p([], pivot, [], result, _), do: [pivot | result]
  defp bubble_p([], pivot, _, result, [pivot | result]), do: bubble_p([], pivot, [], result, [])

  defp bubble_p([], pivot, origin = [head | tail], result, _),
    do: bubble_p(tail, head, [], [pivot | result], origin)

  defp bubble_p([head | tail], pivot, traversed, result, origin) when pivot < head,
    do: bubble_p(tail, head, [pivot | traversed], result, origin)

  defp bubble_p([head | tail], pivot, traversed, result, origin),
    do: bubble_p(tail, pivot, [head | traversed], result, origin)

  @doc """
  Sort a given-method enumerable, using InsertionSort.

  Ascending unless giving the second argument true.

  ## Examples

      iex> Theopse.Algorithms.Sort.insertion([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0])
      [0, 0, 2, 2, 4, 4, 4, 4, 5, 7, 7]

      iex> Theopse.Algorithms.Sort.insertion([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0], true)
      [7, 7, 5, 4, 4, 4, 4, 2, 2, 0, 0]

  """
  @spec insertion(list(term)) :: list(term)
  @spec insertion(list(term), boolean) :: list(term)
  def insertion(enum, desc? \\ false)
  def insertion(enum, desc?), do: bubble(enum, desc?)

  @doc """
  Sort a given-method enumerable, using MergeSort.

  Ascending unless giving the second argument true.

  ## Examples

      iex> Theopse.Algorithms.Sort.merge([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0])
      [0, 0, 2, 2, 4, 4, 4, 4, 5, 7, 7]

      iex> Theopse.Algorithms.Sort.merge([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0], true)
      [7, 7, 5, 4, 4, 4, 4, 2, 2, 0, 0]

  """
  @spec merge(list(term)) :: list(term)
  @spec merge(list(term), boolean) :: list(term)
  def merge(enum, desc? \\ false)
  def merge(enum, false), do: merge_p(enum)
  def merge(enum, true), do: merge(enum, false) |> Enum.reverse()

  dpws merge_p(item) do
    len = trunc(length(item) / 2)

    {left, right} = Enum.split(item, len)
    merge_p(merge_p(right), merge_p(left), [])
  end

  # Count the time may be not a good choice
  # Enum.reverse could be fast
  # Maybe I'll find a better way to handle it
  defp merge_p([], [], result), do: Enum.reverse(result)
  defp merge_p([lhead | ltail], [], result), do: merge_p(ltail, [], [lhead | result])
  defp merge_p([], [rhead | rtail], result), do: merge_p([], rtail, [rhead | result])

  defp merge_p([lhead | ltail], right = [rhead | _], result) when lhead < rhead,
    do: merge_p(ltail, right, [lhead | result])

  defp merge_p(left, [rhead | rtail], result), do: merge_p(left, rtail, [rhead | result])

  @doc """
  Sort a given-method enumerable, using QuickSort.

  Ascending unless giving the second argument true.

  ## Examples

      iex> Theopse.Algorithms.Sort.quick([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0])
      [0, 0, 2, 2, 4, 4, 4, 4, 5, 7, 7]

      iex> Theopse.Algorithms.Sort.quick([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0], true)
      [7, 7, 5, 4, 4, 4, 4, 2, 2, 0, 0]

  """
  @spec quick(list(term)) :: list(term)
  @spec quick(list(term), boolean) :: list(term)
  def quick(enum, desc? \\ false)
  def quick(enum, false), do: quick(enum, true) |> Enum.reverse()
  def quick(enum, true), do: quick_p(enum)

  dpw(quick_p([pivot | rest]), do: quick_p(rest, pivot, [], []))

  defp quick_p([], pivot, less, more), do: Enum.concat([quick_p(more), [pivot], quick_p(less)])

  defp quick_p([head | tail], pivot, less, more) when head < pivot,
    do: quick_p(tail, pivot, [head | less], more)

  defp quick_p([head | tail], pivot, less, more), do: quick_p(tail, pivot, less, [head | more])
end
