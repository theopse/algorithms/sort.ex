# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File is From Theopse Organization(mail@theopse.org)
# Licensed under BSD-2-Clause
# File: macros.ex (theopse/existence/algorithms/sort.ex/lib/sort/macros.ex)
# Content:
# Copyright (c) 2023 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defmodule Theopse.Algorithms.Sort.Macros do
  @moduledoc """
  Sorting Macros
  """

  use Theopse.Mininal

  defmacro dpw(item = {name, _, _}, do: block) do
    dpwc(item, name, block, quote(do: b > a))
  end

  defmacro dpws(item = {name, _, _}, do: block) do
    dpwc(item, name, block, quote(do: b < a))
  end

  defmacro fortest(commit, func) do
    time = Enum.random(10..20)

    for sort <- [:quick, :merge, :insertion, :bubble] do
      quote do
        test "#{unquote(commit)}(#{unquote(to_string(sort))})" do
          Range.new(1, unquote(time))
          |> Enum.each(fn _ ->
            unquote(func).(
              &(Theopse.Algorithms.Sort.unquote(sort) / 1),
              &Theopse.Algorithms.Sort.unquote(sort)(&1, true)
            )
          end)
        end
      end
    end
    |> Enum.reduce(&Theopse.Mininal.O.macro_concat/2)
  end

  defp dpwc(item, name, block, content) do
    quote do
      defp unquote(name)([]), do: []
      defp unquote(name)([item]), do: [item]
      defp unquote(name)([a, b]) when unquote(content), do: [b, a]
      defp unquote(name)([a, b]), do: [a, b]
      defp unquote(item), do: unquote(block)
    end
  end
end
