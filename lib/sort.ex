# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File is From Theopse Organization(mail@theopse.org)
# Licensed under BSD-2-Clause
# File: sort.ex (theopse/existence/algorithms/sort.ex/sort.ex/lib/sort.ex)
# Content:
# Copyright (c) 2023 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defprotocol Theopse.Algorithms.Sort do
  @moduledoc """
  Sorting algorithms by Theopse Organization
  """

  use Theopse.Mininal

  @doc """
  Sort a given-method enumerable, using BubbleSort.

  Ascending unless giving the second argument true.

  ## Examples

      iex> Theopse.Algorithms.Sort.bubble([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0])
      [0, 0, 2, 2, 4, 4, 4, 4, 5, 7, 7]

      iex> Theopse.Algorithms.Sort.bubble([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0], true)
      [7, 7, 5, 4, 4, 4, 4, 2, 2, 0, 0]

  """
  @spec bubble(t) :: t
  @spec bubble(t, boolean) :: t
  def bubble(enum, desc? \\ false)

  @doc """
  Sort a given-method enumerable, using InsertionSort.

  Ascending unless giving the second argument true.

  ## Examples

      iex> Theopse.Algorithms.Sort.insertion([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0])
      [0, 0, 2, 2, 4, 4, 4, 4, 5, 7, 7]

      iex> Theopse.Algorithms.Sort.insertion([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0], true)
      [7, 7, 5, 4, 4, 4, 4, 2, 2, 0, 0]

  """
  @spec insertion(t) :: t
  @spec insertion(t, boolean) :: t
  def insertion(enum, desc? \\ false)

  @doc """
  Sort a given-method enumerable, using MergeSort.

  Ascending unless giving the second argument true.

  ## Examples

      iex> Theopse.Algorithms.Sort.merge([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0])
      [0, 0, 2, 2, 4, 4, 4, 4, 5, 7, 7]

      iex> Theopse.Algorithms.Sort.merge([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0], true)
      [7, 7, 5, 4, 4, 4, 4, 2, 2, 0, 0]

  """
  @spec merge(t) :: t
  @spec merge(t, boolean) :: t
  def merge(enum, desc? \\ false)

  @doc """
  Sort a given-method enumerable, using QuickSort.

  Ascending unless giving the second argument true.

  ## Examples

      iex> Theopse.Algorithms.Sort.quick([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0])
      [0, 0, 2, 2, 4, 4, 4, 4, 5, 7, 7]

      iex> Theopse.Algorithms.Sort.quick([7, 4, 4, 2, 0, 7, 4, 4, 2, 5, 0], true)
      [7, 7, 5, 4, 4, 4, 4, 2, 2, 0, 0]

  """
  @spec quick(t) :: t
  @spec quick(t, boolean) :: t
  def quick(enum, desc? \\ false)
end
