# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File is From Theopse Organization(mail@theopse.org)
# Licensed under BSD-2-Clause
# File: sort_test.ex (theopse/existence/algorithms/sort.ex/test/sort_test.ex)
# Content:
# Copyright (c) 2023 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defmodule SortTest do
  use ExUnit.Case
  import Theopse.Algorithms.Sort.Macros
  doctest Theopse.Algorithms.Sort

  fortest("Empty List", fn sort, sort2 ->
    assert sort.([]) == []
    assert sort2.([]) == []
  end)

  fortest("One Element List", fn sort, sort2 ->
    num = Enum.random(10000..99999)
    assert sort.([num]) == [num]
    assert sort2.([num]) == [num]
  end)

  fortest("Two Element List", fn sort, sort2 ->
    num1 = Enum.random(10000..99999)
    num2 = Enum.random(10000..99999)
    list = if num1 > num2, do: [num2, num1], else: [num1, num2]
    assert sort.([num1, num2]) == list
    assert sort2.([num1, num2]) == Enum.reverse(list)
  end)

  fortest("Sorted List", fn sort, sort2 ->
    list = Stream.iterate(0, &(&1 + 1)) |> Enum.take(Enum.random(10..20))
    assert sort.(list) == list
    assert sort2.(list) == Enum.reverse(list)
  end)

  fortest("Reversed List", fn sort, sort2 ->
    num = Enum.random(10..20)
    list = Stream.iterate(num - 1, &(&1 - 1)) |> Enum.take(num)
    assert sort.(list) == Enum.reverse(list)
    assert sort2.(list) == list
  end)

  fortest("Unsorted List", fn sort, sort2 ->
    num = Enum.random(10..20)
    excepted = Stream.iterate(0, &(&1 + 1)) |> Enum.take(num)
    origin = Enum.shuffle(excepted)
    assert sort.(origin) == excepted
    assert sort2.(origin) == Enum.reverse(excepted)
  end)

  fortest("Two-Last Unordered List", fn sort, sort2 ->
    num = Enum.random(10..20)
    list = Stream.iterate(num - 1, &(&1 - 1)) |> Enum.take(num)
    assert sort.([num, num + 1 | list] |> Enum.reverse()) == Enum.reverse([num + 1, num | list])
    assert sort2.([num, num + 1 | list] |> Enum.reverse()) == [num + 1, num | list]
  end)

  fortest("Two-First Unordered List", fn sort, sort2 ->
    num = Enum.random(10..20)
    list = Stream.iterate(2, &(&1 + 1)) |> Enum.take(num)
    assert sort.([1, 0 | list]) == [0, 1 | list]
    assert sort2.([1, 0 | list]) == Enum.reverse([0, 1 | list])
  end)
end
