# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File is From Theopse Organization(mail@theopse.org)
# Licensed under BSD-2-Clause
# File: .formatter.exs (theopse/existence/algorithms/sort.ex/.formatter.exs)
# Content: Elixir format config
# Copyright (c) 2023 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

# Used by "mix format"
[
  inputs: ["{mix,.formatter}.exs", "{config,lib,src,test}/**/*.{ex,exs}"]
]
